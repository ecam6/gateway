# Gateway Pick to light

Ce projet appartient au projet [pick to light](https://gitlab.com/ecam6/ptl_apps) et se base sur [Python](https://www.python.org/).

La gateway recoit les messages du mapper donc avec toutes les informations dont la gateway a besoin.

Son rôle est d'envoyer les messages aux différents modules de sortie disponible.

- Pour l'IOLink (IFM) par exemple, un message HTTP est envoyé au maître contenant le nouveau status des LEDs.
- Pour les ESP un message MQTT est envoyé avec la command associé à la led.


# Documentation

`Broker.py` contient les éléments de connexion et reconnexion au message broker.

`main.py` est le point d'entrée du programme, connexion, on_message, et gestion des ESPs

`Factory.py` correspond à tous les controleurs qui ne sont pas des ESP (IoLink...)

# Variable d'environement

- BROKER_HOST (default: broker)
- BROKER_PORT (default: 15672)
- BROKER_USER (default: guest)
- BROKER_PWD (default: guest)

# Déploiement, usage, configuration...

Ne peut pas être utilisé seul, se référé au [projet principal](https://gitlab.com/ecam6/ptl_apps)
