import os
import pika
import json
import logging

import Broker
import Factory
import requests

QUEUE_MAPPED_GTW = "/ptl/mapped/gateway"


def esp(channel, in_message):
    # Searching ESP on open queuess
    r = requests.get(
        "http://"
        + os.getenv("BROKER_HOST", "broker")
        + ":"
        + os.getenv("BROKER_PORT", "15672")
        + "/api/queues",
        auth=(os.getenv("BROKER_USER", "guest"), os.getenv("BROKER_PWD", "guest")),
        verify=False,
    )
    esp_found = False
    esp_name = "esp-ptl-" + str(in_message["workstation_pos"])
    for queue in r.json():
        if esp_name in queue["name"]:
            esp_found = True
    if not esp_found:
        logging.info("[x] no esp found for <" + esp_name + ">")
        return False
    # ESP Found build the payload & send the message
    topic = (
        esp_name + ".light.led" + str(in_message["nb_bac"]) + ".command"
    )  # Build the topic
    state = "ON" if in_message["state"] == True else "OFF"  # Build the state
    rgb = tuple(
        int(in_message["color"].lstrip("#")[i : i + 2], 16) for i in (0, 2, 4)
    )  # Build the color in rdb from hex
    payload = (
        "{'state': "
        + state
        + ", 'brightness':"
        + str(in_message["brightness"])
        + ", 'color': {'r': "
        + str(rgb[0])
        + ", 'g': "
        + str(rgb[1])
        + ", 'b': "
        + str(rgb[2])
        + " }}"
    )  # Build the payload
    channel.basic_publish(exchange="amq.topic", routing_key=topic, body=payload)
    logging.info(
        "[x] Sending MQTT message : queue <" + topic + "> data " + str(payload) + " ESP"
    )


def on_message(
    channel, method_frame, header_frame, body
):  # Fonction appelé à chaque message reçu
    try:
        in_message = json.loads(body)
        logging.info(
            "[-] Receive MQTT message : queue <"
            + QUEUE_MAPPED_GTW
            + "> data "
            + str(in_message)
        )
        if (
            "controller" in in_message
        ):  # Si le message concerne un contrôleur type IoLink
            Factory.ControllerFactory().create_controller(in_message).send()
        else:  # Je n'ai pas de controlleur particulier alors on essaie avec un ESP
            esp(channel, in_message)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
    except Exception as e:
        logging.error("ERROR : <" + e.__class__.__name__ + "> : " + str(e))


if __name__ == "__main__":
    RABBIT_MQ_URL = os.getenv("BROKER_HOST", "broker")
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(message)s",
        level=logging.INFO,
        datefmt="[%d/%m/%Y %H:%M:%S]",
    )

    channel, connection = Broker.Setup(RABBIT_MQ_URL, [QUEUE_MAPPED_GTW])

    channel.basic_consume(QUEUE_MAPPED_GTW, on_message)
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        logging.info("[MAIN] STOP process due to keyboard interrupt")
        channel.stop_consuming()
    connection.close()
    logging.info("[MAIN] Bye")
