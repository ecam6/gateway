import requests
import logging
import abc


class ControllerFactory:
    def create_controller(self, in_message):
        # Reflection call constructor
        _class = globals()[in_message["controller"]["type"]]
        return _class(in_message)


class Controller:
    in_message = None

    def __init__(self, in_message):
        self.in_message = in_message

    @abc.abstractmethod
    def send(self):
        pass


class iolink(Controller):
    def compute_pin(self, data, pin, state, std_port_nb):
        pin_bin = list(
            format(int(data[pin]), "06b")
        )  # Get pin state numeric to binary code
        pin_bin[-(std_port_nb + 1)] = str(int(state))  # Change the right code state
        data[pin] = str(int("".join(pin_bin), 2))  # Reverse binary to numeric
        return data

    def computedata(self, std_port_nb_mul, data, pin1, pin2, state, color):
        # Only have 3 color...
        # compute pin depends on the color
        std_port_nb = std_port_nb_mul // 2
        data = list(data)
        if color == 0:
            data = self.compute_pin(data, pin1, state, std_port_nb)
            data = self.compute_pin(data, pin2, False, std_port_nb)
        elif color == 1:
            data = self.compute_pin(data, pin1, False, std_port_nb)
            data = self.compute_pin(data, pin2, state, std_port_nb)
        else:
            data = self.compute_pin(data, pin1, state, std_port_nb)
            data = self.compute_pin(data, pin2, state, std_port_nb)

        return "".join(data)

    def send(self):
        # Get actuel state in order to not overide actual state
        base_url = "http://" + self.in_message["controller"]["ip"]
        partial_url = (
            "/iolinkmaster/port["
            + str(self.in_message["controller"]["port"])
            + "]/iolinkdevice/pdout"
        )
        r = requests.get(base_url + partial_url + "/getdata")
        r.raise_for_status()
        code = r.json()["code"]
        if code == 503:
            raise Exception(
                "Iolink slave is unplugged got 503 response from Iolink master"
            )
        elif code != 200:
            raise Exception(
                "Iolink can't communicate with slave... got error code <"
                + str(code)
                + ">"
            )
        # Apply the change to the state
        data = r.json()["data"]["value"]
        slave_port = self.in_message["controller"]["slave_port"]
        if (slave_port % 2) == 0:
            data = self.computedata(
                slave_port,
                data,
                1,
                5,
                self.in_message["state"],
                self.in_message["color"],
            )  # only for odd port (0,2,4...)
        else:
            data = self.computedata(
                slave_port - 1,
                data,
                3,
                7,
                self.in_message["state"],
                self.in_message["color"],
            )  # only for even port (1,3,5...)
        # Send message
        request_data = {
            "code": "request",
            "cid": 1502,
            "adr": partial_url + "/setdata",
            "data": {"newvalue": data},
        }
        r = requests.post(base_url, json=request_data)
        r.raise_for_status()
        logging.info(
            "[x] Sending HTTP message : ip <"
            + self.in_message["controller"]["ip"]
            + "> data "
            + str(request_data)
            + " IOLINK"
        )
